<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>El cuaderno de preguntas</title>

<link href="assets/css/profile.css" rel="stylesheet">
<link href="webjars/bootstrap/3.3.7-1/css/bootstrap.min.css"	rel="stylesheet">

</head>
<body>


<div class="container">
		<div class="header clearfix">
			<nav>
			<ul class="nav nav-pills pull-right">

				<c:choose>
					<c:when test="${not empty sessionScope.profesorLoggedIn }">
						<jsp:include page="includes/menu_logged.jsp">
							<jsp:param value="inicio" name="inicio" />
							<jsp:param name="usuario"
								value="${sessionScope.profesorLoggedIn.nombre }" />
						</jsp:include>
					</c:when>
					<c:otherwise>
						<jsp:include page="includes/menu.jsp">
							<jsp:param value="inicio" name="inicio" />
						</jsp:include>
					</c:otherwise>
				</c:choose>
			</ul>
			</nav>
			<h3 class="text-muted">Blog Core</h3>
		</div>
	
	
			<div class="row col-lg-8 col-lg-offset-2 text-center">
					<h3>Formulario de acceso</h3>
					<form:form id="login-form" action="/login" method="post"
					 role="form" autocomplete="off" modelAttribute="profesorLogin">
						<div class="form-group">
							<div class="form-group">
								<form:input type="email" name="email" id="email" tabindex="1"	 
								class="form-control" path="email" 
								placeholder="Correo Electronico" required="required" 
								autofocus="autofocus" />
							</div>
							<div class="form-group">
								<form:input type="password" name="password" id="password" 
								tabindex="2" class="form-control" path="password" 
								placeholder="Contraseña" required="required" />
							</div>
							<div class="form-group">
								<button type="submit" name="login-submit" id="login-submit" 
								tabindex="3" class="btn btn-lg btn-primary btn-block">Entrar</button>
							</div>
						</div>
					</form:form>
				</div>

			<c:if test="${not empty error}">
			<div class="row">
				<div class="col-lg-8 col-lg-offset-2">
					<div class="alert alert-danger alert-dismissible fade in"
						role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<c:out value="${error}"></c:out>
					</div>
				</div>
			</div>
		</c:if>
	
		
	<script src="webjars/jquery/3.1.1/jquery.min.js"></script>
	<script src="webjars/bootstrap/3.3.7-1/js/bootstrap.min.js"></script>

</body>
</html>