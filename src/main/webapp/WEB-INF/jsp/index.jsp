<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>El cuaderno de preguntas</title>

<link href="assets/css/profile.css" rel="stylesheet">
<link href="webjars/bootstrap/3.3.7-1/css/bootstrap.min.css" rel="stylesheet">
<link href="//netdna.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

</head>
<body>

<div class="container">
		<div class="header clearfix">
			<nav>
			<ul class="nav nav-pills pull-right">

				<c:choose>
					<c:when test="${not empty sessionScope.profesorLoggedIn }">
						<jsp:include page="includes/menu_logged.jsp">
							<jsp:param value="inicio" name="inicio" />
							<jsp:param name="usuario"
								value="${sessionScope.profesorLoggedIn.nombre }" />
						</jsp:include>
					</c:when>
					<c:otherwise>
						<jsp:include page="includes/menu.jsp">
							<jsp:param value="inicio" name="inicio" />
						</jsp:include>
					</c:otherwise>
				</c:choose>
			</ul>
			</nav>
			<h3 class="text-muted">Blog Core</h3>
		</div>
	</div>


	<div class="contentme">
		<c:forEach items="${preguntas}" var="preguntas">
			<br>
			<p class="lead"><a href="/post/${preguntas.id}">${preguntas.contenido }</a></p>
			<br />
			<hr>
		</c:forEach>
	</div>
	
		
	<script src="/webjars/jquery/3.1.1/jquery.min.js"></script>
	<script src="/webjars/bootstrap/3.3.7-1/js/bootstrap.min.js"></script>

</body>
</html>