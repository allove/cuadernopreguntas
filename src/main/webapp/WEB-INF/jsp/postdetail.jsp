<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>El cuaderno de preguntas</title>

<link href="/assets/css/profile.css" rel="stylesheet">
<link href="/webjars/bootstrap/3.3.7-1/css/bootstrap.min.css"	rel="stylesheet">

</head>
<body>


<div class="container">
		<div class="header clearfix">
			<nav>
			<ul class="nav nav-pills pull-right">

				<c:choose>
					<c:when test="${not empty sessionScope.profesorLoggedIn }">
						<jsp:include page="includes/menu_logged.jsp">
							<jsp:param value="inicio" name="inicio" />
							<jsp:param name="usuario"
								value="${sessionScope.profesorLoggedIn.nombre }" />
						</jsp:include>
					</c:when>
					<c:otherwise>
						<jsp:include page="includes/menu.jsp">
							<jsp:param value="inicio" name="inicio" />
						</jsp:include>
					</c:otherwise>
				</c:choose>
			</ul>
			</nav>
			<h3 class="text-muted">Blog Core</h3>
		</div>
	
	<div>
	<h1>${preguntas.contenido }</h1>
	</div>
		
	<div class="RESPUESTASFOREACH">
		<c:forEach items="${preguntas.respuestas }" var="respuestas">
			<p>${respuestas.id }</p>
		</c:forEach>
	</div>
	
	
	<div>
		<h4>Respuestas:</h4>
					<c:choose>
						<c:when test="${not empty sessionScope.profesorLoggedIn}">

							<form:form method="POST" modelAttribute="respuestaForm"
								id="form-comment" action="/submit/newRespuesta" role="form">
								<form:input type="hidden" id="pregunta_id" name="pregunta_id" path="pregunta_id" value="${pregunta.id}" />
								<div class="input-group">
									<form:input type="text" class="form-control input-sm chat-input" placeholder="Escribe tu comentario aquí" path="contenido" />
									<span class="input-group-btn" id="comment-button"> 
								<a href="#" class="btn btn-primary btn-sm">
									<span class="glyphicon glyphicon-comment"></span> Comentar
								</a>
							</span>
								</div>
							</form:form>

						</c:when>
						<c:otherwise>
							<h5>Necesita iniciar sesión para poder crear respuestas.</h5>
						</c:otherwise>
					</c:choose>
					<hr data-brackets-id="12673">
					<ul data-brackets-id="12674" id="sortable" class="list-unstyled ui-sortable">
					
					
							<c:forEach items="${preguntas.respuestas}" var="respuesta">
								<li class="ui-state-default"><strong
									class="pull-left primary-font">${respuesta.profesor.nombre}</strong>
									 <br /> ${respuesta.contenido} <br /></li>
							</c:forEach>
					</ul>
					</div>
				</div>
	
	
		
	<script src="/webjars/jquery/3.1.1/jquery.min.js"></script>
	<script src="/webjars/bootstrap/3.3.7-1/js/bootstrap.min.js"></script>
<script>
		$(document).ready(function() {
		    $("#comment-button").click(function() {
				$("#form-comment").submit();
		    });
		});
		
		</script>


</body>
</html>