<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>El cuaderno de preguntas</title>

<link href="assets/css/profile.css" rel="stylesheet">
<link href="webjars/bootstrap/3.3.7-1/css/bootstrap.min.css"	rel="stylesheet">

</head>
<body>


<div class="container">
		<div class="header clearfix">
			<nav>
			<ul class="nav nav-pills pull-right">

				<c:choose>
					<c:when test="${not empty sessionScope.profesorLoggedIn }">
						<jsp:include page="includes/menu_logged.jsp">
							<jsp:param value="inicio" name="inicio" />
							<jsp:param name="usuario"
								value="${sessionScope.profesorLoggedIn.nombre }" />
						</jsp:include>
					</c:when>
					<c:otherwise>
						<jsp:include page="includes/menu.jsp">
							<jsp:param value="inicio" name="inicio" />
						</jsp:include>
					</c:otherwise>
				</c:choose>
			</ul>
			</nav>
			<h3 class="text-muted">Blog Core</h3>
		</div>
	
		
		
		<div class="col-lg-8 col-lg-offset-2" >
			<div class="text-center">
				<h3>Registro</h3>
					<form:form id="register-form" action="/register" method="post" role="form" autocomplete="off" modelAttribute="profesorRegister" >
						<div class="form-group">
						<div class="form-group"> <form:input type="text" name="nombre"  id="nombre" tabindex="1" class="form-control" path="nombre" placeholder="Nombre" /></div>
						<div class="form-group"> <form:input type="email" name="email"  id="email" tabindex="2" class="form-control" path="email" placeholder="Correo Electronico" /></div>
						<div class="form-group"> <form:input type="password" name="password"  id="password" tabindex="3" class="form-control" path="password" placeholder="Contraseña" /></div>
						<div class="form-group"> <form:input type="password" name="secondPassword"  id="secondPassword" tabindex="4" class="form-control" path="secondPassword" placeholder="Repite la contraseña" data-rule-equalTo="#password" /></div>
						<div class="form-group"> <input type="submit" name="register-submit"  id="register-submit" tabindex="5" class="form-control btn btn-info" value="Registrar ahora" /></div>
					</div>
					
					 </form:form>
				
			</div>
		</div>
		</div>
	
	
	
		
	<script src="webjars/jquery/3.1.1/jquery.min.js"></script>
	<script src="webjars/bootstrap/3.3.7-1/js/bootstrap.min.js"></script>
		<script src="assets/js/jquery.validate.min.js"></script>
	<script src="assets/js/messages_es.js"></script>
	<script>
			$(document).ready(function() {
				$("#register-form").validate()
				});
	</script>

</body>
</html>