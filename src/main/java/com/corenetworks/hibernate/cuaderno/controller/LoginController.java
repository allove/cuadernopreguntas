package com.corenetworks.hibernate.cuaderno.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.corenetworks.hibernate.cuaderno.beans.LoginBean;
import com.corenetworks.hibernate.cuaderno.dao.ProfesorDao;
import com.corenetworks.hibernate.cuaderno.model.Profesores;

@Controller
public class LoginController {
	@Autowired
	private ProfesorDao profesorDao;
	
	@Autowired
	private HttpSession httpSession;

	@GetMapping(value="/signin")
	public String signin(Model modelo) {
		modelo.addAttribute("profesorLogin", new LoginBean());
		
		return "login";
	}
	
	@PostMapping(value="/login")
	public String submitLogin(@ModelAttribute("profesorLogin") LoginBean l, Model modelo) {
		Profesores p = profesorDao.getByEmailAndPassword(l.getEmail(), l.getPassword());
		if (p != null) {
			httpSession.setAttribute("profesorLoggedIn", p);
			return "redirect:/";
		} else {
			modelo.addAttribute("error", "Error al introducir el usuario o la contraseña");
			return "login";
		}
	}
	
	@GetMapping(value="/logout")
	public String logout(Model modelo) {
		httpSession.removeAttribute("profesorLoggedIn");
		return "redirect:/";
	}
}
