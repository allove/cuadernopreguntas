package com.corenetworks.hibernate.cuaderno.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.corenetworks.hibernate.cuaderno.beans.PreguntaBean;
import com.corenetworks.hibernate.cuaderno.beans.RespuestaBean;
import com.corenetworks.hibernate.cuaderno.dao.PreguntasDao;
import com.corenetworks.hibernate.cuaderno.dao.RespuestaDao;
import com.corenetworks.hibernate.cuaderno.model.Preguntas;
import com.corenetworks.hibernate.cuaderno.model.Profesores;
import com.corenetworks.hibernate.cuaderno.model.Respuestas;

@Controller
public class PreguntasController {
	@Autowired
	private PreguntasDao preguntaDao;
	
	@Autowired
	private RespuestaDao respuestaDao;
	
	@Autowired
	private HttpSession httpSession;
	
	@GetMapping(value="/submit")
	public String showForm(Model modelo) {
		modelo.addAttribute("pregunta", new PreguntaBean());
		return "submit";
	}
	
	@PostMapping(value="/submit/newpregunta")
	public String submit(@ModelAttribute("pregunta") PreguntaBean preguntabean, Model modelo) {
		Preguntas pregunta01 = new Preguntas();
		pregunta01.setContenido(preguntabean.getContenido());
		
		Profesores profesor = (Profesores) httpSession.getAttribute("profesorLoggedIn");
		pregunta01.setAutor(profesor);
		preguntaDao.create(pregunta01);
		profesor.getPreguntas().add(pregunta01);
		
		return "redirect:/";
	}
	
	@GetMapping(value="/post/{id}")
	public String detail(@PathVariable("id") long id, Model modelo) {
		Preguntas result = null;
		if ((result = preguntaDao.getById(id)) != null) {
			modelo.addAttribute("preguntas", result);
			modelo.addAttribute("respuestaForm", new RespuestaBean());
			return "postdetail";
		} else {
			return "redirect:/";
		}
}
	
	@PostMapping(value="/submit/newRespuesta")
	public String submitrespuesta(@ModelAttribute("respuestaForm") RespuestaBean respuestaBean, Model model ) {
		Profesores profesor = (Profesores) httpSession.getAttribute("profesorLoggedIn");
		
		Respuestas respuesta = new Respuestas();
		respuesta.setProfesor(profesor);
		
		Preguntas pregunta = preguntaDao.getById(respuestaBean.getPregunta_id());
		respuesta.setPregunta(pregunta);
		respuesta.setContenido(respuestaBean.getContenido());
		respuestaDao.create(respuesta);
		pregunta.getRespuestas().add(respuesta);
		profesor.getRespuestas().add(respuesta);
		
		return "redirect:/post/" + respuestaBean.getPregunta_id();
	}
}
