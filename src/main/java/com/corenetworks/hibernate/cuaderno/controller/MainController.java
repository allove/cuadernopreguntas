package com.corenetworks.hibernate.cuaderno.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.corenetworks.hibernate.cuaderno.dao.PreguntasDao;

@Controller
public class MainController {
	
	@Autowired
	private PreguntasDao preguntaDao;
	
	@GetMapping(value="/")
	public String listarPreguntas(Model modelo) {
		modelo.addAttribute("preguntas", preguntaDao.getAll());
		
		return "index";
	}
	
}
