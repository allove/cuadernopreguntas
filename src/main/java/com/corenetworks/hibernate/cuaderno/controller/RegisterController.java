package com.corenetworks.hibernate.cuaderno.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.corenetworks.hibernate.cuaderno.beans.RegisterBean;
import com.corenetworks.hibernate.cuaderno.dao.ProfesorDao;
import com.corenetworks.hibernate.cuaderno.model.Profesores;

@Controller
public class RegisterController {
	@Autowired
	private ProfesorDao profesorDao;
	
	
	@GetMapping(value="/signup")
	public String signup(Model modelo) {
		modelo.addAttribute("profesorRegister", new RegisterBean());
		return "register";
	}
	
	@RequestMapping(value="/create")
	@ResponseBody
	public String create(String nombre, String email, String password) {
		try {
			Profesores profesor01 = new Profesores();
			profesor01.setNombre(nombre);
			profesor01.setEmail(email);
			profesor01.setPassword(password);
			profesorDao.create(profesor01);
		} catch (Exception ex) {
			return "Error creando el usuario: " + ex.toString();
		}
		return "Usuario creado correctamente";
	}
	
	@PostMapping(value="/register")
	public String submit(@ModelAttribute("profesorRegister") RegisterBean r, Model modelo) {
		profesorDao.create(new Profesores(r.getNombre(), r.getEmail(), r.getPassword()));
		
		return "redirect:/autores";
	}
}
