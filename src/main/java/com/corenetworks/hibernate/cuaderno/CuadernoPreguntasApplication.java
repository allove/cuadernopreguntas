package com.corenetworks.hibernate.cuaderno;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CuadernoPreguntasApplication {

	public static void main(String[] args) {
		SpringApplication.run(CuadernoPreguntasApplication.class, args);
	}
}
