package com.corenetworks.hibernate.cuaderno.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

@Entity
public class Respuestas {
	@Id
	@GeneratedValue
	private long id;
	
	@Column
	@Lob
	private String contenido;
	
	@ManyToOne
	@JoinColumn(name="profesor_id", updatable=false)
	private Profesores profesor;
	
	@ManyToOne
	@JoinColumn(name="pregunta_id", updatable=false)
	private Preguntas pregunta;

	public Respuestas() {
		super();
	}

	public Respuestas(String contenido, Profesores profesor, Preguntas pregunta) {
		super();
		this.contenido = contenido;
		this.profesor = profesor;
		this.pregunta = pregunta;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getContenido() {
		return contenido;
	}

	public void setContenido(String contenido) {
		this.contenido = contenido;
	}

	public Profesores getProfesor() {
		return profesor;
	}

	public void setProfesor(Profesores profesor) {
		this.profesor = profesor;
	}

	public Preguntas getPregunta() {
		return pregunta;
	}

	public void setPregunta(Preguntas pregunta) {
		this.pregunta = pregunta;
	}
	
	
}
