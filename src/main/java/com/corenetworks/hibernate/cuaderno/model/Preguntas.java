package com.corenetworks.hibernate.cuaderno.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Preguntas {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	@Column
	@Lob
	private String contenido;
	
	@ManyToOne
	private Profesores profesor;
	
@OneToMany(mappedBy="pregunta")
private List<Respuestas>  respuestas = new ArrayList<>();
	
	public Preguntas() {
	}
	
	public Preguntas(String contenido) {
		super();
		this.contenido = contenido;
	}

	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public String getContenido() {
		return contenido;
	}


	public void setContenido(String contenido) {
		this.contenido = contenido;
	}

	public Profesores getAutor() {
		return profesor;
	}

	public void setAutor(Profesores autor) {
		this.profesor = autor;
	}

	public List<Respuestas> getRespuestas() {
		return respuestas;
	}

	public void setRespuestas(List<Respuestas> respuestas) {
		this.respuestas = respuestas;
	}
	
	
	
}
