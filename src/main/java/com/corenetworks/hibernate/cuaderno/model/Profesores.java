package com.corenetworks.hibernate.cuaderno.model;

import java.util.Set;

import java.util.HashSet;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.hibernate.annotations.ColumnTransformer;

@Entity
public class Profesores {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	@Column(name= "NAME")
	private String nombre;
	@Column
	private String email;
	@Column
	@ColumnTransformer(write = " MD5(?) ")
	private String password;
	
	@OneToMany(mappedBy="profesor", fetch = FetchType.EAGER)
	private Set<Preguntas> preguntas = new HashSet<>();
	
	@OneToMany(mappedBy = "profesor", fetch = FetchType.EAGER)
	private Set<Respuestas> respuestas = new HashSet<>();
	
	public Profesores() {
		super();
	}

	public Profesores(String nombre, String email, String password) {
		super();
		this.nombre = nombre;
		this.email = email;
		this.password = password;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Set<Preguntas> getPreguntas() {
		return preguntas;
	}

	public void setPreguntas(Set<Preguntas> preguntas) {
		this.preguntas = preguntas;
	}

	public Set<Respuestas> getRespuestas() {
		return respuestas;
	}

	public void setRespuestas(Set<Respuestas> respuestas) {
		this.respuestas = respuestas;
	}
	
	
	
}
