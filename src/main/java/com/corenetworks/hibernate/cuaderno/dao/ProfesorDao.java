package com.corenetworks.hibernate.cuaderno.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.corenetworks.hibernate.cuaderno.model.Profesores;

@Repository
@Transactional
public class ProfesorDao {

	@PersistenceContext
	private EntityManager entityManager;
	
	public void create(Profesores profesor) {
		entityManager.persist(profesor);
	}
	
	@SuppressWarnings("unchecked")
	public List<Profesores> getAll(){
		return entityManager.createQuery("select u from Profesores u").getResultList();
	}
	
	public Profesores getByEmailAndPassword(String email, String password) {
		Profesores resultado = null;
		try {
			resultado = (Profesores) entityManager.createNativeQuery("select * FROM Profesores where  email= :email and password=md5(:password)", Profesores.class)
					.setParameter("email", email)
					.setParameter("password", password)
					.getSingleResult();
		} catch(NoResultException e) {
			resultado = null;
		}
		return resultado;
	}
	
}
