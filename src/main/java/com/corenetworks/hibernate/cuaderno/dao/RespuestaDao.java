package com.corenetworks.hibernate.cuaderno.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.corenetworks.hibernate.cuaderno.model.Respuestas;

@Repository
@Transactional
public class RespuestaDao {

	@PersistenceContext
	private EntityManager entityManager;
	
	public void create(Respuestas respuesta) {
		entityManager.persist(respuesta);
	}
	
	@SuppressWarnings("unchecked")
	public List<Respuestas> getAll(){
		return entityManager.createQuery("select r from Respuestas r").getResultList();
	}
	
	public Respuestas getById(long id) {
		return entityManager.find(Respuestas.class, id);
	}
	
}
