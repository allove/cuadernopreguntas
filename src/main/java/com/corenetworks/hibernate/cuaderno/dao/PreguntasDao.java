package com.corenetworks.hibernate.cuaderno.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.corenetworks.hibernate.cuaderno.model.Preguntas;

@Repository
@Transactional
public class PreguntasDao {

	@PersistenceContext
	private EntityManager entityManager;
	
	public void create(Preguntas preguntas) {
		entityManager.persist(preguntas);
	}
	
	@SuppressWarnings("unchecked")
	public List<Preguntas> getAll(){
		return entityManager.createQuery("select p from Preguntas p").getResultList();
	}
	
	public Preguntas getById(long id) {
		return entityManager.find(Preguntas.class, id);
	}
}
