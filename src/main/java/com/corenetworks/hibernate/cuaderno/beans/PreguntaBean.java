package com.corenetworks.hibernate.cuaderno.beans;

public class PreguntaBean {
	private String contenido;
	private long id;
	
	public PreguntaBean() {
		id=-1;
	}

	public String getContenido() {
		return contenido;
	}

	public void setContenido(String contenido) {
		this.contenido = contenido;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	
}
